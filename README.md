# HelixNLEK

**HelixNLEK** is a Split **N**on-**L**iner **E**diting **K**eyboard inspried by [DaVinci Resolve Speed Editor](https://www.blackmagicdesign.com/products/davinciresolve/keyboard), [MakotoKurauchi](https://github.com/MakotoKurauchi)'s [Helix](https://github.com/MakotoKurauchi/helix) Keyboard and [Scott Bezek](https://github.com/scottbez1)'s [smartknob](https://github.com/scottbez1/smartknob).

This project is still under heavy development and it's also my first hardware project so use it at yout own risk.

## Keyboard Layout

```
      0      1      2      3      4      5      6      7
   ,--------------------.             ,--------------------.
 0 |  M0  |  M1  |  M2  |             |   M15   ||   M16   |
   |------+------+------|             |---------++---------|
 1 |  M3  |  M4  |  M5  |             |  M17 |  M18 |  M19 |
   |---------++---------|             |------+------+------|
 2 |  Cut In || Cut Out |             |  EL0 |  EL1 |  EL2 |
   |---------++---------+-------------+------+------+------|
 3 |  M6  |  M7  |  M8  |  L0  |  L3  |                    |
   |------+------+------+------+------+                    |
 4 |  M9  |  M10 |  M11 |  L1  |  L4  |   ←     M20    →   |
   |------+------+------+------+------+                    |
 5 |  M12 |  M13 |  M14 |  L2  |  L5  |   Rotary Encoder   |
   `-------------------------------------------------------'
```
